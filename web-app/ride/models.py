from django.contrib.auth.models import Permission, User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
import django.utils.timezone as timezone

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    phone = models.CharField(max_length=20, blank=True)
    is_driver = models.BooleanField(default=False,null=True)
    vehicle_type = models.CharField(max_length=20, blank=True, null=True)
    vehicle_license = models.CharField(max_length=20, blank=True, null=True)
    vehicle_max_passenger = models.IntegerField(blank=True, null=True, default=0)
    vehicle_special_info = models.CharField(max_length=150, blank=True, null=True)

    @receiver(post_save, sender=User)
    def created_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class OrderInfo(models.Model):
    created_time = models.DateTimeField(auto_now_add=True,null=True, blank=True)
    owner_id = models.CharField(max_length=80, blank=True)
    sharer_id = models.CharField(max_length=80, blank=True, null=True)
    status = models.CharField(max_length=40)
    starting_address = models.CharField(max_length=150)
    destination_address = models.CharField(max_length=150)
    new_starting_address = models.CharField(max_length=150,null=True)
    new_destination_address = models.CharField(max_length=150,null=True)
    share_permission = models.BooleanField(null=True)
    required_arrival_date = models.DateField(default=timezone.now)
    required_arrival_time = models.TimeField(default=timezone.now)
    preferred_vehicle_type = models.CharField(max_length=30, blank=True, null=True)
    driver_id = models.CharField(max_length=80, blank=True, null=True)
    special_requests = models.CharField(max_length=150, blank=True, null=True)
    number_of_passengers = models.IntegerField(default=1)
    driver_status = models.CharField(max_length=30,default='NONE')
    number_of_ownerpassengers = models.IntegerField(default=1)
    dirvers_car_capacity = models.IntegerField(null=True)
    remain_space = models.IntegerField(null=True)
    canceled = models.BooleanField(null=True)

    def __str(self):
        return self.created_time
