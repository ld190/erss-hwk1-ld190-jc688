from django import forms
from django.contrib.auth.models import User

from .models import Profile, OrderInfo




class  RegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password', 'email']


class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ['phone', 'is_driver', 'vehicle_type', 'vehicle_license', 'vehicle_special_info', \
                  'vehicle_max_passenger']


class OrderForm(forms.ModelForm):

    class Meta:
        model = OrderInfo
        fields = ['starting_address', 'destination_address', 'share_permission', 'required_arrival_date', \
                  'required_arrival_time', 'preferred_vehicle_type', 'special_requests', 'number_of_ownerpassengers']


class SharerForm(forms.ModelForm):
    number_of_passengers = forms.IntegerField(widget=forms.NumberInput)

    class Meta:
        model = OrderInfo
        fields = ['number_of_passengers']

class SearchFormDriver(forms.ModelForm):

    class Meta:
        model = OrderInfo
        fields = ['starting_address', 'destination_address' , 'required_arrival_date', 'preferred_vehicle_type']


class SearchFormSharer(forms.ModelForm):

    class Meta:
        model = OrderInfo
        fields = ['starting_address', 'destination_address', 'required_arrival_date', 'number_of_passengers']

class OwnerupdateForm(forms.ModelForm):

    class Meta:
        model = OrderInfo
        fields = ['new_starting_address', 'new_destination_address', 'share_permission', 'required_arrival_date', \
                  'required_arrival_time', 'preferred_vehicle_type', 'special_requests', 'number_of_passengers', 'canceled']

class SharerupdatedForm(forms.ModelForm):

    class Meta:
        model = OrderInfo
        fields = ['number_of_passengers', 'canceled']