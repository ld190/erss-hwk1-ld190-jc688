from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'ride'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('register/', views.register, name = 'register'),
    path('login_user/', views.login_user, name = 'login_user'),
    path('logout_user/', views.logout_user, name = 'logout_user'),
    url(r'^create_order/$', views.create_order, name = 'create_order'),
    url(r'^update_profile/$', views.update_profile, name = 'update_profile'),
    url(r'^(?P<order_id>[0-9]+)/update_order/$', views.update_order, name= 'update_order'),
    url(r'^profile_detail/$',views.profile_detail, name = 'profile_detail'),
    url(r'^driversearch/$', views.driversearch, name = 'driversearch'),
    url(r'^(?P<order_id>[0-9]+)/detail/$', views.detail, name='detail'),
    url(r'^(?P<order_id>[0-9]+)/driverdetail/$', views.driverdetail, name='driverdetail'),
    url(r'^(?P<order_id>[0-9]+)/sharer_ask/$', views.sharer_ask, name='sharer_ask'),
    url(r'^sharersearch/$', views.sharersearch, name='sharersearch'),
    url(r'^(?P<order_id>[0-9]+)/driver_choose/$', views.driver_choose, name='driver_choose'),
    url(r'^(?P<order_id>[0-9]+)/complete/$', views.complete, name='complete'),

]