from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django import forms
from .models import *
from django.db.models import Q
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from .forms import ProfileForm, OrderForm, RegisterForm, SharerForm, SearchFormDriver, SearchFormSharer, OwnerupdateForm,  SharerupdatedForm
from .models import Profile, OrderInfo
from django.http import JsonResponse
from django.contrib.auth.models import Permission, User
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.conf import settings
# Create your views here.

def index(request):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
        shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
        drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
        return render(request, 'ride/index.html', {'created_orders': created_orders, 'shared_orders': shared_orders,
                                                   'drived_orders': drived_orders})


def register(request):
    form = RegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
                shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
                drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
                return render(request, 'ride/index.html',
                              {'created_orders': created_orders, 'shared_orders': shared_orders,
                               'drived_orders': drived_orders})
    context = {
        "form": form,
    }
    return render(request, 'ride/register.html', context)


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
                shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
                drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
                return render(request, 'ride/index.html',
                              {'created_orders': created_orders, 'shared_orders': shared_orders,
                               'drived_orders': drived_orders})

            else:
                return render(request, 'ride/login.html', {'error_message': 'Invalid login'})
    return render(request, 'ride/login.html')


def logout_user(request):
    logout(request)
    form = ProfileForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'ride/login.html', context)


def detail(request, order_id):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        user = request.user
        order = get_object_or_404(OrderInfo, pk=order_id)
        return render(request, 'ride/detail.html', {'order': order, 'user': user})


def create_order(request):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        form = OrderForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            order = form.save(commit=False)
            order.owner_id = request.user.username
            order.special_requests = request.POST['special_requests']
            if request.POST['number_of_ownerpassengers']:
                order.number_of_passengers = request.POST['number_of_ownerpassengers']
                order.number_of_ownerpassengers = request.POST['number_of_ownerpassengers']
            else:
                order.number_of_passengers = 1
                order.number_of_ownerpassengers = 1
            order.destination_address = request.POST['destination_address']
            order.required_arrival_date = request.POST['required_arrival_date']
            order.required_arrival_time = request.POST['required_arrival_time']

            order.preferred_vehicle_type = request.POST['preferred_vehicle_type']
            if request.POST['share_permission'] == '2':
                order.share_permission = True
                order.status = 'waiting'
            else:
                order.share_permission = False
                order.status = 'waiting'
            order.number_of_ownerpassengers = request.POST['number_of_ownerpassengers']
            order.save()
            created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
            shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
            drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
            return render(request, 'ride/index.html',
                          {'created_orders': created_orders, 'shared_orders': shared_orders,
                           'drived_orders': drived_orders})

    context = {
        "form": form,
    }

    return render(request, 'ride/create_order.html', context)


def update_profile(request):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        form = ProfileForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            user = request.user
            user.profile.phone = request.POST['phone']
            if request.POST['is_driver'] == '2':
                user.profile.is_driver = True
            else:
                user.profile.is_driver = False
            if request.POST['vehicle_max_passenger']:
                user.profile.vehicle_max_passenger = request.POST['vehicle_max_passenger']
            user.profile.vehicle_license = request.POST['vehicle_license']
            user.profile.vehicle_type = request.POST['vehicle_type']
            user.profile.vehicle_special_info = request.POST['vehicle_special_info']
            user.save()
            return render(request,'ride/profile_detail.html',{'user':user})

    context = {
        'form':form,
    }

    return render(request, 'ride/update_profile.html', context)


def profile_detail(request):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        user = request.user
        return render(request, 'ride/profile_detail.html', {'user':user})

def sharersearch(request):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        form = SearchFormSharer(request.POST or None, request.FILES or None)
        if form.is_valid():
            orders = OrderInfo.objects.filter(starting_address=request.POST['starting_address'])
            orders = orders.filter(destination_address=request.POST['destination_address'])
            orders = orders.filter(required_arrival_date=request.POST['required_arrival_date'])
            orders = orders.filter(status='waitingsharer')
            number = int(request.POST['number_of_passengers'])
            for num in range(0,number):
                orders = orders.exclude(remain_space=num)
            return render(request, 'ride/join_order.html', {'shareorders':orders})

    context = {
        'form':form,
    }
    return render(request, 'ride/sharersearch.html', context)

def sharer_ask(request,order_id):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        form = SharerForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            order = OrderInfo.objects.get(pk=order_id)
            valid_number = order.dirvers_car_capacity - order.number_of_passengers
            if int(request.POST['number_of_passengers']) <= valid_number:
                order.number_of_passengers += int(request.POST['number_of_passengers'])
                order.sharer_id = request.user.username
                order.remain_space = order.dirvers_car_capacity - order.number_of_passengers
                order.status = 'sharerjoined'
                order.save()
                email = request.user.email
                send_mail('Order joined', 'At ubber, we use the capacity of car to filter your search result.\n A order must be confirmed by driver first\n You will not receive a confrimed email again, however you joined this order successfully now!', settings.DEFAULT_FROM_EMAIL,
                          [email], fail_silently=False)
                created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
                shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
                drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
                return render(request, 'ride/index.html',
                             {'created_orders': created_orders, 'shared_orders': shared_orders,
                              'drived_orders': drived_orders})
            else:
                form = SearchFormSharer(request.POST or None, request.FILES or None)
                if form.is_valid():
                    orders = OrderInfo.objects.filter(starting_address=request.POST['starting_address'])
                    orders = orders.filter(destination_address=request.POST['destination_address'])
                    orders = orders.filter(required_arrival_date=request.POST['required_arrival_date'])
                    orders = orders.filter(status='waitingsharer' or 'waiting')
                    return render(request, 'ride/join_order.html', {'shareorders': orders})

            context = {
                'form': form,
            }
            return render(request, 'ride/sharersearch.html', context)

    context = {
        'form':form,
    }

    return render(request, 'ride/sharer_ask.html', context)


def driversearch(request):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        user = request.user
        #all = OrderInfo.objects.filter(driver_id=request.user.username)
        #for each in all:
            #each.delete()
        if not user.profile.is_driver:
            created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
            shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
            drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
            return render(request, 'ride/index.html',
                          {'created_orders': created_orders, 'shared_orders': shared_orders,
                           'drived_orders': drived_orders})
        form = SearchFormDriver(request.POST or None, request.FILES or None)
        if form.is_valid():
            orders = OrderInfo.objects.filter(starting_address=request.POST['starting_address'])
            orders = orders.filter(destination_address=request.POST['destination_address'])
            orders = orders.filter(required_arrival_date=request.POST['required_arrival_date'])
            if request.POST['preferred_vehicle_type']:
                orders = orders.filter(preferred_vehicle_type=request.POST['preferred_vehicle_type'])
            orders = orders.filter(status='waiting')
            return render(request, 'ride/accept_orders.html', {'waitingorders': orders})

    context = {
        'form':form,
    }
    return render(request, 'ride/driversearch.html', context)

def driver_choose(request, order_id):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        order = OrderInfo.objects.get(pk=order_id)
        driver = request.user
        if order.number_of_passengers < driver.profile.vehicle_max_passenger:
            if order.share_permission:
                order.status = 'waitingsharer'
            else:
                order.status = 'comfirmed'
            order.remain_space = driver.profile.vehicle_max_passenger - order.number_of_ownerpassengers
            order.dirvers_car_capacity = driver.profile.vehicle_max_passenger
            order.driver_id = request.user.username
            order.save()
            toman = User.objects.filter(username=order.owner_id)
            for each in toman:
                email = each.email
                send_mail('Order Confirmed','Your order has been confirmed by a driver!',settings.DEFAULT_FROM_EMAIL,[email],fail_silently=False)
            created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
            shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
            drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
            return render(request, 'ride/index.html',
                        {'created_orders': created_orders, 'shared_orders': shared_orders,
                        'drived_orders': drived_orders})
        else:
            created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
            shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
            drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
            return render(request, 'ride/index.html',
                          {'created_orders': created_orders, 'shared_orders': shared_orders,
                           'drived_orders': drived_orders})


def update_order(request, order_id):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        order = OrderInfo.objects.get(pk=order_id)
        if order.status == 'waiting' and order.owner_id == request.user.username:
            form = OwnerupdateForm(request.POST or None, request.FILES or None)
            if form.is_valid():
                if request.POST['number_of_passengers']:
                    order.number_of_ownerpassengers = request.POST['number_of_passengers']
                    order.number_of_passengers = order.number_of_ownerpassengers
                if request.POST['new_destination_address']:
                    order.destination_address = request.POST['new_destination_address']
                if request.POST['new_starting_address']:
                    order.starting_address = request.POST['new_starting_address']
                order.required_arrival_date = request.POST['required_arrival_date']
                order.required_arrival_time = request.POST['required_arrival_time']
                order.preferred_vehicle_type = request.POST['preferred_vehicle_type']
                if request.POST['share_permission'] == '2':
                    order.share_permission = True
                    order.status = 'waiting'
                else:
                    order.share_permission = False
                    order.status = 'waiting'
                order.save()
                if request.POST['canceled'] == '2':
                    order.delete()
                created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
                shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
                drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
                return render(request, 'ride/index.html',
                            {'created_orders': created_orders, 'shared_orders': shared_orders,
                                    'drived_orders': drived_orders})
            context = {'form':form,}
            return render(request, 'ride/update_order.html', context)
        if order.status == 'sharerjoined' and order.sharer_id == request.user.username:
            form2 = SharerupdatedForm(request.POST or None, request.FILES or None)
            if form2.is_valid():
                order.number_of_passengers = order.number_of_ownerpassengers + int(request.POST['number_of_passengers'])
                order.save()
                if request.POST['canceled'] == '2':
                    order.sharer_id = ''
                    order.number_of_passengers = order.number_of_ownerpassengers
                    order.remain_space = order.dirvers_car_capacity - order.number_of_ownerpassengers
                    order.save()
                created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
                shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
                drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
                return render(request, 'ride/index.html',
                              {'created_orders': created_orders, 'shared_orders': shared_orders,
                               'drived_orders': drived_orders})
            context = {'form': form2, }
            return render(request, 'ride/sharerupdate.html', context)
        else:
            thisuser = request.user
            thisorder = get_object_or_404(OrderInfo, pk=order_id)
            return render(request, 'ride/detail.html', {'order': thisorder, 'user': thisuser})

    context = {
        "form": form,
    }

    return render(request, 'ride/update_order.html', context)


def driverdetail(request, order_id):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        user = request.user
        order = get_object_or_404(OrderInfo, pk=order_id)
        return render(request, 'ride/driverdetail.html', {'order': order, 'user': user})

def complete(request, order_id):
    if not request.user.is_authenticated:
        return render(request, 'ride/login.html')
    else:
        order = OrderInfo.objects.get(pk = order_id)
        order.status = 'completed'
        order.save()
        created_orders = OrderInfo.objects.filter(owner_id=request.user.username)
        shared_orders = OrderInfo.objects.filter(sharer_id=request.user.username)
        drived_orders = OrderInfo.objects.filter(driver_id=request.user.username)
        return render(request, 'ride/index.html',
                      {'created_orders': created_orders, 'shared_orders': shared_orders,
                       'drived_orders': drived_orders})


